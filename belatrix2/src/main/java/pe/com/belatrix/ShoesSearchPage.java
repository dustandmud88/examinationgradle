package pe.com.belatrix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

import utiles.Constants;
import utiles.UtilitariosButton;
import utiles.UtilitariosCheckBox;
import utiles.UtilitariosTextField;
import utiles.UtilitariosWebElement;

import org.openqa.selenium.support.FindBy;

public class ShoesSearchPage {
	WebDriver driver;
	
	@FindBy (className = "x-searchable-list__textbox__aspect-Brand")
	WebElement txtBoxBrand_Search;
	
	@FindBy (xpath = "//span[contains(text(),'PUMA')]/../input[@type = 'checkbox']")
	WebElement chkBox_Puma;
	
	@FindBy (xpath = "//a[@class='srp-carousel-list__item-link']/div[contains(text(),'PUMA')]")
	WebElement lbl_filter_Puma;
	
	@FindBy (xpath = "//span[@class='cbx x-refine__multi-select-cbx' and text() ='10']/../input[@type = 'checkbox']")
	WebElement chkBox_filter_Size10;
	
	@FindBy (className = "srp-controls__count-heading")
	WebElement lbl_NumberOfResults;
	
	@FindBy (xpath = "//span[text() ='Precio + Envío: más bajo primero']/..")
	WebElement lbl_FilterAscendantPriceOrder;
	
	@FindBy (xpath = "//h3[@class = 's-item__title']/..")
	List <WebElement> listOfResult; 
	
	@FindBy (xpath = "//h3[@class = 's-item__title']/../h3")
	List <WebElement> listOfResultArticlesText; 
	
	@FindBy (xpath = "//a[contains(text(),'¡Cómpralo ahora!')]")
	WebElement btnCompraloAhora;
	
	@FindBy (id = "isCartBtn_btn")
	WebElement btnAddItemToCart;
	
	@FindBy (id = "sbin-gxo-btn")
	WebElement btnCompleteAsAGuest;
	
	@FindBy (id = "GREET-HELLO")
	WebElement lblHello;
	
	@FindBy (xpath = "//option[contains(text(),'10') or contains(text(),'43')]")
	WebElement optionShoesSize;
	
	@FindBy (className = "PSEUDOLINK")
	WebElement btnSeeAllResults;
	
	@FindBy (xpath = "//div[@class='srp-controls--selected-value']")
	WebElement btnHover;
	
	@FindBy (id = "itemTitle")
	WebElement articleName; 
	
	@FindBy (id = "prcIsum")
	WebElement articlePrice;
	
	@FindBy (xpath = "//span[@id='fshippingCost']/span[1]")
	WebElement shippingPrice;
	
	public ShoesSearchPage (WebDriver driver) {
		  this.driver = driver;
		  PageFactory.initElements(driver, this);  	  
	
	}
	
	public void searchForSpecificBrandShoesFilter(String searchBrand,ITestResult testResult) {
		UtilitariosTextField.txtField_clean(txtBoxBrand_Search, driver, testResult);
		UtilitariosTextField.txtField_putStringJS(searchBrand, txtBoxBrand_Search, driver, testResult);
		UtilitariosCheckBox.btn_clicChkBoxJS(chkBox_Puma, driver, testResult);
		
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.visibilityOf(lbl_filter_Puma));
		
	}
	
	public void selectSizeShoesFilter10(ITestResult testResult) {
		UtilitariosCheckBox.btn_clicChkBoxJS(chkBox_filter_Size10, driver, testResult);
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.visibilityOf(lbl_NumberOfResults));
		
	}
	
	public void showInConsoleResultsSearch() {	
		String chain = "The number of results for Puma's shoes size ten: "+lbl_NumberOfResults.getText();
		System.out.println(chain);
		Constants.bufferReport = Constants.bufferReport + chain +"<br>";
	}
	
	public void orderResultsByPriceAscendant(ITestResult testResult) {

		Actions builder = new Actions(driver);
		builder.moveToElement(btnHover).perform();
		
		UtilitariosButton.btn_clicConJSIsClickable(lbl_FilterAscendantPriceOrder, driver, testResult);

		//We must take into consideration that the articles are order in ascendant
		//way  by the amount represented by article plus shipping price
	}
	
	public void select5Items(int numeroItem,ITestResult testResult) {
		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("window.open()");
		

		Set handles = driver.getWindowHandles();
		Iterator iterator = handles.iterator();
		
		String url = listOfResult.get(numeroItem).getAttribute("href");
		String primeraVentana = (String) iterator.next();
		String segundaVentana = (String) iterator.next();
		driver.switchTo().window(segundaVentana);
		driver.get(url);
		UtilitariosWebElement.escroleaHastaWebElementJS(optionShoesSize, driver);
		UtilitariosButton.btn_clic(optionShoesSize, driver, testResult);
		
		printArticleConsole();
		
		UtilitariosWebElement.escroleaHastaWebElementJS(btnAddItemToCart, driver);
		UtilitariosButton.btn_clicConJSIsClickable(btnAddItemToCart, driver, testResult);
		
		driver.close();
		driver.switchTo().window(primeraVentana);
		
	}
	
	public void printArticleConsole() {
		System.out.println("ArticleName   :  "+articleName.getText());
		Constants.bufferReport = Constants.bufferReport + "ArticleName   :  "+articleName.getText()+"<br>";
		System.out.println("ArticlePrice  :  "+articlePrice.getText());
		Constants.bufferReport = Constants.bufferReport + "ArticlePrice  :  "+articlePrice.getText()+"<br>";
		System.out.println("ShippingPrice :  "+shippingPrice.getText()+"\n");
		Constants.bufferReport = Constants.bufferReport + "ShippingPrice :  "+shippingPrice.getText()+"<br>"+"<br>";
	}


	public void orderByBNameAscendantAndPrintArticles(int size) {
		String arrayOfNames [] = new String [size];

		for (int i = 0; i< size; i++) {
			arrayOfNames[i] = listOfResultArticlesText.get(i).getText();
		}
		

		for (int i = 0 ; i <arrayOfNames.length-1 ;i++) {
			for (int j = i+1 ; j<arrayOfNames.length;j++) {
				if (arrayOfNames[i].compareTo(arrayOfNames[j])>0){
					String temporal = arrayOfNames[i];
					arrayOfNames[i] = arrayOfNames[j];
					arrayOfNames[j] = temporal;
				}
			}
		}
		
		//print the articles values in ascendant order
		System.out.println("Articles in ascendant order"+"<br>");
		Constants.bufferReport = Constants.bufferReport + "Articles in ascendant order"+"<br>";
		for (int i = 0 ; i<arrayOfNames.length;i++) {	
			System.out.println("Article "+(i+1)+" : "+arrayOfNames[i]);
			Constants.bufferReport = Constants.bufferReport + "Article "+(i+1)+" : "+arrayOfNames[i]+"<br>";
		}
		Constants.bufferReport = Constants.bufferReport +"<br>";
		System.out.println("\n");
		
	}
	
	public void orderByBNameDescendantAndPrintArticles(int size) {
		String arrayOfNames [] = new String [size];

		for (int i = 0; i< size; i++) {
			arrayOfNames[i] = listOfResultArticlesText.get(i).getText();
		}
		

		for (int i = arrayOfNames.length-1 ; i > 0;i--) {
			for (int j = i-1 ; j > -1;j--) {
				if (arrayOfNames[i].compareTo(arrayOfNames[j])>0){
					String temporal = arrayOfNames[i];
					arrayOfNames[i] = arrayOfNames[j];
					arrayOfNames[j] = temporal;
				}
			}
		}
		
		//print the articles values in ascendant order
		System.out.println("Articles in descendant order");
		Constants.bufferReport = Constants.bufferReport + "Articles in descendant order"+"<br>";
		for (int i = 0 ; i<arrayOfNames.length;i++) {	
			System.out.println("Article "+(i+1)+" : "+arrayOfNames[i]);
			Constants.bufferReport = Constants.bufferReport + "Article "+(i+1)+" : "+arrayOfNames[i]+"<br>";
		}
		Constants.bufferReport = Constants.bufferReport +"<br>";
		System.out.println("\n");
		
	}
	
}
