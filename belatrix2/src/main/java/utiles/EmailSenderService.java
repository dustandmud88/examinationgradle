package utiles;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.JSONArray;
import org.json.JSONObject;

 


/* Tomado de y modificado
 * https://www.adictosaltrabajo.com/2008/12/01/javamail/
 * */

public class EmailSenderService {
	private final Properties properties = new Properties();
	private Session session;
	private void init(String emailSender) {
		properties.put("mail.smtp.user","belatrixtest88@gmail.com");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port","587");	
		properties.put("mail.smtp.auth","true");
		properties.put("mail.smtp.starttls.enable","true");
		properties.put("mail.smtp.mail.sender",emailSender);
		
		session = Session.getDefaultInstance(properties);
	}
 
	
	public void sendEmailToOnePlusAttachtment(String emailSender, String passwordSender, String emailDestiny, String subject, String body
			,String pathAttachtment, String fileNameAttachtment){
		 
		init(emailSender);
		try{
			 MimeMessage message = new MimeMessage(session);
			 message.setFrom(new InternetAddress((String)properties.get("mail.smtp.mail.sender")));
			 message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailDestiny));
			 message.setSubject(subject);
			
		     BodyPart messageBodyPart = new MimeBodyPart();
	         messageBodyPart.setText(body);
			 
	         Multipart multipart = new MimeMultipart();
	         multipart.addBodyPart(messageBodyPart);
	         //se crea el otro fragmento para agregar el archivo y se agrega a multipart
	         messageBodyPart = new MimeBodyPart();
	         DataSource source = new FileDataSource(pathAttachtment);
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         messageBodyPart.setFileName(fileNameAttachtment);
	         multipart.addBodyPart(messageBodyPart);

	         message.setContent(multipart);
			
			
			 Transport t = session.getTransport("smtp");
			 t.connect((String)properties.get("mail.smtp.host"),(String)properties.get("mail.smtp.user"), passwordSender);
			 t.sendMessage(message, message.getAllRecipients());
			 t.close();
		}catch (MessagingException me){
                        //Aqui se deberia o mostrar un mensaje de error o en lugar
                        //de no hacer nada con la excepcion, lanzarla para que el modulo
                        //superior la capture y avise al usuario con un popup, por ejemplo.
			System.out.println("Error al enviar correo de prueba. ");
			me.printStackTrace();
			return;
		}
		
	}
		
	
	
 
}
