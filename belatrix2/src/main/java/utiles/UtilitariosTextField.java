package utiles;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

public class UtilitariosTextField {

	// TextField - Campo de texto
	//Limpiar campo de texto
	public static void txtField_clean (WebElement textField, WebDriver driver, ITestResult testResult ){
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.visibilityOf(textField));
		try {
			textField.clear();	
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}				
	}
	//Llenar campo de texto sin JS
	public static void txtField_putString (String valor,WebElement textField, WebDriver driver, ITestResult testResult ){
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeClickable(textField));
		try {
			
			JavascriptExecutor js = (JavascriptExecutor) driver;		
	        js.executeScript("arguments[0].scrollIntoView(true);", textField);
			textField.sendKeys(valor);	 
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}	
	}
	//Llenar campo de texto con JS
	public static void txtField_putStringJS (String valor,WebElement textField, WebDriver driver, ITestResult testResult ){
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeClickable(textField));
		try {
			
			JavascriptExecutor js = (JavascriptExecutor) driver;		
	        js.executeScript("arguments[0].scrollIntoView(true);", textField);
	        js.executeScript("arguments[0].value='"+valor+"';", textField);
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}	
	}
	public static void txtField_sendKey (Keys key,WebElement textField, WebDriver driver, ITestResult testResult ){
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeClickable(textField));
		try {
			
			textField.sendKeys(key);
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}	
	}
	
	public static void ingresarTextoNumerico(String cadena, WebElement webElement, WebDriver driver, ITestResult testResult) {
		
		for (int i = 0; i<cadena.length();i++ ) {
			
			if(cadena.charAt(i) == '1') {
				UtilitariosTextField.txtField_sendKey(Keys.NUMPAD1, webElement, driver, testResult);
			}else {
				if(cadena.charAt(i) == '2') {
					UtilitariosTextField.txtField_sendKey(Keys.NUMPAD2, webElement, driver, testResult);
				}else {
					if(cadena.charAt(i) == '3') {
						UtilitariosTextField.txtField_sendKey(Keys.NUMPAD3, webElement, driver, testResult);
					}else {
						if(cadena.charAt(i) == '4') {
							UtilitariosTextField.txtField_sendKey(Keys.NUMPAD4, webElement, driver, testResult);
						}else {
							if(cadena.charAt(i) == '5') {
								UtilitariosTextField.txtField_sendKey(Keys.NUMPAD5, webElement, driver, testResult);
							}else {
								if(cadena.charAt(i) == '6') {
									UtilitariosTextField.txtField_sendKey(Keys.NUMPAD6, webElement, driver, testResult);
								}else {
									if(cadena.charAt(i) == '7') {
										UtilitariosTextField.txtField_sendKey(Keys.NUMPAD7, webElement, driver, testResult);
									}else {
										if(cadena.charAt(i) == '8') {
											UtilitariosTextField.txtField_sendKey(Keys.NUMPAD8, webElement, driver, testResult);
										}else {
											if(cadena.charAt(i) == '9') {
												UtilitariosTextField.txtField_sendKey(Keys.NUMPAD9, webElement, driver, testResult);
											}else {
												if(cadena.charAt(i) == '0') {
													UtilitariosTextField.txtField_sendKey(Keys.NUMPAD0, webElement, driver, testResult);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
		
	}

	public static  String getTexto(WebElement textField) {
		
		return textField.getText();
	}
	public static  String getTextoAttributeValue(WebElement textField, String name ) {
		
		return textField.getAttribute(name);
	}
	

}
