package utiles;



public class Constants {
	
	//ruta del archivo de configuración Correo
	//navegador a probar
	public final static String AUT_navegador = "chrome";  // firefox, chrome, edge, opera or  internet explorer
	
	public final static String pathMail = System.getProperty("user.dir")+"\\src\\main\\resources\\correo\\correo.properties";
	
	//URL's 
	public final static String url_AUT = "https://www.ebay.com/";
	
	public static String bufferReport = "";
	
	//Constante de tiempo de espera del webdriver en esperar la carga de un elemento
	public static final long  tiempo_segundosEspera = 50;
	
	public static final long  tiempo_milisegundosIntentarCada = 250;  //para el fluent web driver
	public static final long  tiempo_milisegundosIntentarCada_2 = 1000;  //para el fluent web driver
	public static final long  tiempo_segundosHastIntervalo = 2;     //para el fluent web drive
}