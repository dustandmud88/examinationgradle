package utiles;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

public class UtilitariosCheckBox {

	
	public static void btn_clicChkBoxJS(WebElement chkBox, WebDriver driver, ITestResult testResult){
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeClickable(chkBox));
		if (!chkBox.isSelected()) {
			try {
				JavascriptExecutor executor = (JavascriptExecutor)driver; 
				executor.executeScript("arguments[0].click();", chkBox); 
			}catch (Exception e){
				UtilitariosReporte.stackTracePrintException(e,testResult);
			}			
		}		
	} 
}
