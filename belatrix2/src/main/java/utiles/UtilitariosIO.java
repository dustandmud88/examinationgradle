package utiles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class UtilitariosIO {

	//Convertir el archivo css a String
	 public static String retornarString () throws IOException{
		 	String path = "";	
		 	if(OSValidator.isWindows() ) {
		 		path = System.getProperty("user.dir")+"\\src\\main\\resources\\configuracionReporte\\customize.css";
		 	}else {
		 		if(OSValidator.isUnix()) {
		 			path = System.getProperty("user.dir")+"/src/main/resources/configuracionReporte/customize.css";
		 		}
		 	}
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line;
			StringBuilder sb = new StringBuilder();
			
			while((line=br.readLine())!= null){ 
			    sb.append(line.trim());
			}	
			br.close();
			return sb.toString();
		}
	  public static String readAll(Reader rd) throws IOException {
		    StringBuilder sb = new StringBuilder();
		    int cp;
		    while ((cp = rd.read()) != -1) {
		      sb.append((char) cp);
		    }
		    return sb.toString();
		  }
	  public static String desencriptarBase64 (String cadena){
		  String decodificar = "";
		  byte[] bytes;
			try {
				  bytes = cadena.getBytes("UTF-8");							  
				  byte[] decoded = Base64.getDecoder().decode(bytes);
				  decodificar = new String (decoded,"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			  
		  return decodificar;	  
	  }

	  

	
}
