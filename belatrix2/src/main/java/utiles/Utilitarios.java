package utiles;

import org.testng.Assert;
import org.testng.ITestResult;

public class Utilitarios {

	
	public static void  stackTracePrintException(Exception e, ITestResult testResult) {
	    StackTraceElement[] stack = e.getStackTrace();
	    String exception = "<pre>";
	    if(e.getMessage() !=null) {
	    	exception = exception+e.getMessage()+"<br>";
	    }
	    for (StackTraceElement s : stack) {
	        exception = exception + s.toString() + "<br>";
	    }
	    exception =  exception + "</pre>";
		Assert.fail(exception); 
		testResult.setAttribute("message",exception);
		e.printStackTrace();
		
	}
}
