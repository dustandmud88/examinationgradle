package utiles;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class UtilitariosWebElement {
	//Scroll - escrolear hasta que se ubique al elemento dentro de la pantalla
	public static void escroleaHastaWebElement(WebElement elemento, WebDriver driver) {		
    	Actions escrolear = new Actions (driver);
    	escrolear.moveToElement(elemento);
	}
	
	public static void escroleaHastaWebElementJS(WebElement elemento, WebDriver driver) {		
    	JavascriptExecutor executor = (JavascriptExecutor)driver; 
    	executor.executeScript("arguments[0].scrollIntoView();", elemento);
	}
	
    
	
	//Esperar tanto segundos hasta que aparezca el elemento
	public static void esperarAparezcaWebElement (WebElement elemento, WebDriver driver) {
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.visibilityOf(elemento));
	}
	
	//verifica si el elemento fue cargado en pantalla
	public static boolean isVisible( WebDriver driver, WebElement element) {
		boolean fueCargado = false;
		
		if(element.isDisplayed()) {
			new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.visibilityOf(element));
			fueCargado = true;
		}
		
		return fueCargado;
	}

	public static String  obtenerElemento(String llave, List<Map<String, Object>> arreglo){
		for (Map<String, Object> elemento: arreglo){
				if(elemento.get(llave) !=null) {
					return elemento.get(llave).toString();
				}else {
					return "";
				}
				
			
		}		
		return "";
	}
	
	public static String  obtenerElementoHashMap(String llave, List<HashMap<String, String>> arreglo){
		for (Map<String, String> elemento: arreglo){
				if(elemento.get(llave) !=null) {
					return elemento.get(llave).toString();
				}else {
					return "";
				}
				
			
		}		
		return "";
	}
	
	public static String  obtenerElementoHashMapUlt(String llave, List<HashMap<String, String>> arreglo){
		String rpta = "";
		for (Map<String, String> elemento: arreglo){
				if(elemento.get(llave) !=null) {
					rpta  = elemento.get(llave).toString();
				}		
		}		
		return rpta;
	}
	public static String  obtenerElementoHashMapUltObject(String llave, List<Map<String, Object>> arreglo){
		String rpta = "";
		for (Map<String, Object> elemento: arreglo){
				if(elemento.get(llave) !=null) {
					rpta  = elemento.get(llave).toString();
				}		
		}		
		return rpta;
	}
	public static String  obtenerElementoHashMapUlt2(String llave, Map<String, Object> map){
		String rpta = "";
		
				if(map.get(llave) !=null) {
					rpta  = map.get(llave).toString();
				}		
				
		return rpta;
	}

	
	public static String  obtenerElementoMapXValor(String llave,String valor,String llave2 ,List<Map<String, Object>> arreglo){
		String rpta = "";
		for (Map<String, Object> elemento: arreglo){
				if(elemento.get(llave) !=null && elemento.get(llave).toString().compareTo( valor)==0) {
					return elemento.get(llave2).toString();
				}		
		}		
		return rpta;
	}
	
	  public static boolean compararWebElementTextVSObjectText(WebElement elemento, Object objeto) {
		  boolean rpta = false;
		  if(objeto == null) {
			  if (elemento.getText().compareTo("")==0 ) {
				  rpta = true;
			  }
		  }else {
			  if (elemento.getText().compareTo(objeto.toString())==0 ) {
				  rpta = true;
			  }
			  
		  }
		  return rpta;
	  } 
	  public static boolean escrolearYCompararWebElemenTextVSObjectText(WebElement elemento, Object objeto, WebDriver driver) {
		  boolean rpta = false;
		  UtilitariosWebElement.escroleaHastaWebElement(elemento, driver);
		  String cadena = elemento.getText();
		  if(objeto == null) {
			  if (cadena.compareTo("")==0 ) {
				  rpta = true;
			  }
		  }else {
			  if (cadena.compareTo(objeto.toString())==0 ) {
				  rpta = true;
			  }
			  
		  }
		  return rpta;
	  } 
	  
	
}
