package com.pe.belatrix;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.NoSuchSessionException;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import pe.com.belatrix.MainPage;
import pe.com.belatrix.ShoesSearchPage;
import utiles.Constants;
import utiles.CreateDriver;
import utiles.EmailSenderService;
import utiles.Utilitarios;
import utiles.UtilitariosReporte;

public class ExerciseTest {
	
	@BeforeSuite
	  public void setUp() throws Exception {
		
			//WebDriverManager.chromedriver().proxy("10.237.125.62:8000");
			WebDriverManager.chromedriver().setup();
			ITestResult testResult = Reporter.getCurrentTestResult();	
			Constants.bufferReport = "";
			
	  }
	 @BeforeMethod 
	 public void setDriver (){
		 	
	 		ITestResult testResult = Reporter.getCurrentTestResult();
			try{
				CreateDriver.getInstance().setDriver(Constants.AUT_navegador);
			}catch (Exception e){
				Utilitarios.stackTracePrintException(e,testResult);
			}	 		
			CreateDriver.getInstance().getDriver().get(Constants.url_AUT); 
			CreateDriver.getInstance().getDriver().manage().window().maximize();	
			
	 }
	
	@Test (description = "Exercise of Bellatrix")
	public void  testOfExercise() {
		final ITestResult testResult = Reporter.getCurrentTestResult();
		MainPage  mainPage  = new MainPage (CreateDriver.getInstance().getDriver());
		ShoesSearchPage shoesSearchPage = new ShoesSearchPage (CreateDriver.getInstance().getDriver());
		
		mainPage.searchForText("Shoes", testResult);
		shoesSearchPage.searchForSpecificBrandShoesFilter("PUMA", testResult);
		shoesSearchPage.selectSizeShoesFilter10(testResult);
		shoesSearchPage.showInConsoleResultsSearch();
		shoesSearchPage.orderResultsByPriceAscendant(testResult);
		shoesSearchPage.select5Items(0, testResult);
		shoesSearchPage.select5Items(1, testResult);
		shoesSearchPage.select5Items(2, testResult);
		shoesSearchPage.select5Items(3, testResult);
		shoesSearchPage.select5Items(4, testResult);
		
		//using the bubble sort algorithm for sorting
		shoesSearchPage.orderByBNameAscendantAndPrintArticles(5);
		/* in order to avoid complexity i could have printed the ascendant list
		 * in reverse order (to get the descendant list) but decided to order 
		 * it the classical way. */
		shoesSearchPage.orderByBNameDescendantAndPrintArticles(5);
		
		UtilitariosReporte.cargarMensajeResultadoTestOk(testResult, Constants.bufferReport, true);
	
	}

	
	 @AfterMethod ()
	 public void tearDown (){ 
		 try{
			CreateDriver.getInstance().getDriver().quit();
			
		 }catch( NoSuchSessionException e) {
			 
		 }
	 }
	 @AfterSuite ()
		public void sendEmail() throws FileNotFoundException, IOException {
		 final ITestResult testResult = Reporter.getCurrentTestResult();
		 int status = testResult.getStatus();
		 if (status == ITestResult.SUCCESS) {
			 EmailSenderService sendMail = new EmailSenderService();	
			 Properties mailProperties = new Properties();
			 mailProperties.load(new FileInputStream(Constants.pathMail));
			 String emailSender      =  mailProperties.getProperty("emailSender").toString();	 
			 String passwordSender   = mailProperties.getProperty("passwordSender").toString();
			 String emailDestiny     = mailProperties.getProperty("emailDestiny").toString(); 
			 String subject          = mailProperties.getProperty("subject").toString();
			 String body             = "This is the body of the test e-mail for Belatrix";
			 String pathAttachtment  = System.getProperty("user.dir")+"\\extentReport.html";
			 String fileNameAttachtment  = "Testing Report.html";
			 sendMail.sendEmailToOnePlusAttachtment(emailSender, passwordSender, emailDestiny, subject, body,
					 pathAttachtment, fileNameAttachtment);
		 }

	}
	 
}
